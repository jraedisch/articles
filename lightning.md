---
title: Lightning Quickstart
created_at: 2018-04-24
updated_at: 2018-08-30
canonical: https://raedisch.net/articles/lightning
---
# Lightning Quickstart

This is a quick guide for setting up a Bitcoin Lightning node on `mainnet`. For simplicity, only the Go based implementations are used. Other setups might be easier on resources (e.g. using `bitcoind` instead of `btcd` might sync faster).

I try to show what to do first and provide context second.

__Be aware that you are now entering the danger zone! I tested this tutorial thoroughly but cannot take any responsibility for any lost funds or other harm resulting from following it.__

## Prerequisites

You should

1. be comfortable using `bash`.
2. install [Go](https://golang.org/), set `$GOPATH`, and add `$GOPATH/bin` to `$PATH`.  
If you never heard of Go before, take a moment to look into it!
3. open ports for `btcd` (tcp:8333) and `lnd` (tcp:9735).
4. own some bitcoin.

## Setting up the Bitcoin node (btcd)

1. install btcd:

        $ go get -u github.com/Masterminds/glide
        $ git clone https://github.com/btcsuite/btcd $GOPATH/src/github.com/btcsuite/btcd
        $ cd $GOPATH/src/github.com/btcsuite/btcd
        $ glide install
        $ go install . ./cmd/...

2. run `btcd`:

        $ btcd -u someUserName -P someRandomPassword -b /ssd/with/300/gb --addcheckpoint='optional/checkpoint'

This is a long running process, so you should probably run it in a `screen` session, or better yet using some process manager like `systemd`.

Params `-u` and `-P` are needed so `lnd` can connect to `btcd` later. Use a strong password!

To speed up syncing a little more, you can add a recent checkpoint, e.g. `--addcheckpoint='510873:0000[…]f16a'`. The checkpoint consists of a block along with its hash. You should not blindly copy it but instead validate via multiple sources ([Blockchain explorers](https://blockchain.info/blocks), etc.).

`btcd` should start syncing, validating and indexing the Blockchain to the folder you provided via `-b`. This folder should reside on a large fast SSD, since there will be a lot of disk access.

The process will also use a lot of CPU, memory and bandwidth. If you value time more than money, start up a powerful virtual machine somewhere and scale it down when syncing is done. You may then also switch from SSD to HDD to save cost. I used google compute engine with a high CPU, 8 core instance and 300gb SSD. After 3 days of syncing I scaled down to 1 core and a HDD (by using a snapshot).

## Setting up the Lightning node (lnd)

Let's go ahead and install `lnd`:

    $ go get -u github.com/golang/dep/cmd/dep
    $ go get -u -d github.com/lightningnetwork/lnd
    $ cd $GOPATH/src/github.com/lightningnetwork/lnd
    $ dep ensure -v
    $ go install -v ./...

Now start it and provide the user and password params chosen when starting `btcd` above:

    $ lnd --bitcoin.active --bitcoin.mainnet --btcd.rpcuser=btcdUser --btcd.rpcpass=btcdPassword

This again is a long running process.

If you want other nodes to be able to initiate channels from their sides, depending on where your `lnd` is running, you might either want to use `--nat` (e.g. if your external IP is often changing), or bind to a specific IP and port to listen to: `--listen 1.2.3.4:9735`. In case of my setup I had to use my node's local IP to bind to. Again, make sure that ingress to port `9735` is allowed!

To further encourage people to connect to your node, you might also want to add an alias, e.g. `--alias ln.example.com`, and publish your `--externalip`.

`lnd` comes with a command line interface. Use it to create a bitcoin wallet like this:

    $ lncli create

Follow the instructions and use a strong wallet password.
You will need the seed (and the optional encryption passphrase) in case you want to restore your BTC wallet elsewhere. Make sure to keep a backup in a safe place or you ~~might~~ will lose funds in the future!

Coming next: funding your wallet and opening channels.
