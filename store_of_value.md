---
title: Bitcoin as a Store of Value
created_at: 2018-07-05
updated_at: 2018-08-30
canonical: https://raedisch.net/articles/store_of_value
---
# Bitcoin as a Store of Value

There is a an argument that I do not see made very often supporting picking Bitcoin as the most valuable cryptocurrency. I wonder whether that is because it is stupid, obvious, or something else. Since I am not too knowledgeable in economics I encourage criticism, so I may learn.

The argument in its simplest form goes something like this: if we elect a store of value, that should not change too easily.

Right now, Bitcoin is leading in market capitalization in the cryptocurrency space. I heard that that is a bad metric, but hope it is good enough to make very broad comparisons like "more people bought the first than the second place". Since buy-in is the best vote we have in this space, I would equate Bitcoin's leading market cap with "Bitcoin as a store of value has more supporters than each of the other cryptocurrencies".

Now I can imagine three scenarios in which this could change.

The first one is a severe protocol level security vulnerability that cannot be fixed. This is comparable to someone finding a cheap way to produce gold from lead, and there is nothing much that can be done after that secret is out. I won't discuss it any further.

The second and third scenarios are: Bitcoin could be replaced by a "related" competitor, where everyone can claim a stake according to their Bitcoin holdings at any time, or it could be replaced by an "unrelated" competitor, where this is not the case.

The scenario of the "unrelated" competitor, e.g. Ethereum taking over, I think would lead to a massive flow of capital out of the cryptocurrency economy, since it means that the most valuable store of value can be replaced at any time.

The scenario of a "related" competitor taking over would be less severe, since people would not lose value, as long as they can claim the new asset according to their Bitcoin holdings at any time. In this case the value would be transferred from one store to the next and I would not have to fear to lose out just because I was not fast enough, e.g. taking a long vacation.

I would expect this takeover to play out quite quickly after the competitor goes live, since there would have most likely been a lot of discussions up front and the majority would already support the new solution for some obvious reasons.

So for now it seems to me the only sane alternative to Bitcoin Maximalism is Nocoinism, and my FOMO does not permit that.
